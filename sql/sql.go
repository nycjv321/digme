package sql

func GetExistingMigrations() (string) {
	return `SELECT
  version,
  name,
  path,
  sha256,
  owner,
  ran,
  duration,
  succeeded
FROM migrations
ORDER BY id DESC;`
}

func GetSchemaVersion() (string) {
	return `SELECT version, succeeded
FROM migrations
ORDER BY id DESC
LIMIT 1;`
}
