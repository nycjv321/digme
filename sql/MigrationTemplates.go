package sql

type Templates interface {
	CheckSchema() string
	CreateSchema() string
	RecordMigrations() string
}


type PostgresTemplates struct {}
type SqlLiteTemplates struct {}

func (p *PostgresTemplates) CheckSchema() (string) {
	return `SELECT EXISTS (
   SELECT 1
   FROM   information_schema.tables
   WHERE  table_schema = 'public'
   AND    table_name = $1
);`
}

func (p *PostgresTemplates) CreateSchema() (string) {
return `CREATE TABLE migrations (
  id BIGSERIAL,
  version INT NOT NULL UNIQUE,
  name VARCHAR(300) NOT NULL UNIQUE,
  path VARCHAR(500) NOT NULL UNIQUE,
  sha256 VARCHAR(256) NOT NULL,
  owner VARCHAR(300) NOT NULL,
  ran TIMESTAMP NOT NULL,
  duration FLOAT NOT NULL,
  succeeded BOOLEAN NOT NULL
);`
}

func (p *PostgresTemplates) RecordMigrations() (string) {
return `INSERT INTO migrations (version, name, path, sha256, owner, ran, duration, succeeded) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);`
}


func (p *SqlLiteTemplates) CheckSchema() (string) {
	return `SELECT name FROM sqlite_master WHERE type='table' AND name=?1;`
}

func (p *SqlLiteTemplates) CreateSchema() (string) {
	return `CREATE TABLE IF NOT EXISTS migrations (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  version INT NOT NULL UNIQUE,
  name VARCHAR(300) NOT NULL UNIQUE,
  path VARCHAR(500) NOT NULL UNIQUE,
  sha256 VARCHAR(256) NOT NULL,
  owner VARCHAR(300) NOT NULL,
  ran TIMESTAMP NOT NULL,
  duration FLOAT NOT NULL,
  succeeded BOOLEAN NOT NULL
);`
}

func (p *SqlLiteTemplates) RecordMigrations() (string) {
	return `INSERT INTO migrations (version, name, path, sha256, owner, ran, duration, succeeded) VALUES (?1,?2,?3,?4,?5,?6,?7,?8);`
}