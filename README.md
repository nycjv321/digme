# digme!

A no-frills Database migration library (postgres, sqlite3)

## Configuration

digme is a simple library. It doesn't ask for much. In order to configure it you just need to provide 
the following inputs:

1. host
2. username
3. password
4. port
5. database vendor (e.g. postgres, mysql)
6. database name
7. additional connection options (optional) 
8. path to the migration files.

Currently these values are read from the environment. Future versions will support 
either environment variables or input yml.

### Environment Variables Configuration

The vendor is the only required field. If it isn't defined digme will error out.


1. DATABASE_HOST                  # defaults to "127.0.0.1"
2. DATABASE_USERNAME              # defaults to "postgres" on postgres
3. DATABASE_PASSWORD              # defaults to the username on postgres
4. DATABASE_PORT                  # defaults to 5432 on postgres
5. DATABASE_VENDOR                # required (currently only "postgres" supported)
6. DATABASE_NAME                  # defaults to the username on postgres
7. DATABASE_ADDITIONAL_OPTIONS    # defaults to "sslmode=disable" on postgres
8. DATABASE_MIGRATION_PATH     as # directory to access the database migrations. defaults to ./migrations

## Migration files

digme will run all migration files defined in `$MIGRATION_PATH`. They are ordered 
based on their prefix version which is signified based on a _v_ prefix. For example:

- v2-my-supa-dupa-migration.sql
- v1-initial-schema.sql

v1 will be ran first and then v2. Files not prefixed with _v_ and ending _.sql_ or
missing the .sql suffix are ignored by digme

## Example Usage

    // that's it!
    import (
      _ "digme"
    )

    func main() {
        // your much cooler code goes here
    }