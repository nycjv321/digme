package digme

import (
	"io/ioutil"
	"crypto/sha256"
	"fmt"
	"path"
	"runtime"
	"time"
)


func getRoot() string {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		panic("No caller information")
	}
	return path.Dir(filename)

}

// TODO clean this code up
func (manager *DBManager) readSql(name string) string {
	if buf, err := ioutil.ReadFile(name); err == nil {
		return string(buf)
	} else {
		return name
	}
}

func measure(f func(string) error, args string) (time.Duration, error) {
	start := time.Now()
	err := f(args)
	return time.Since(start), err
}

func getHash(fileName string) string {
	h := sha256.New()
	h.Write([]byte(fileName))
	return fmt.Sprintf("%x", h.Sum(nil))
}
