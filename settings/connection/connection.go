package connection

type Settings interface {
	Get() (vendor string, connectionUrl string)
	GetUserName() string
	GetVendor() string
}
