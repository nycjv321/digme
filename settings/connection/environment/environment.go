package environment

import (
	"gitlab.com/nycjv321/digme/settings/configuration"
	"gitlab.com/nycjv321/digme/settings/connection"
	"gitlab.com/nycjv321/digme/settings/database"
	"gitlab.com/nycjv321/digme/settings/database/postgres"
	"gitlab.com/nycjv321/digme/settings/database/sqlite3"
	"log"
	"os"
)

var sqliteVendor = "sqlite3"
var postgresVendor = "postgres"

func ConnectionSettings() connection.Settings {
	vendor := database.DatabaseVendor()
	switch vendor {
	case postgresVendor:
		return postgresConnectionSettings()
	case sqliteVendor:
		return sqliteConnectionSettings()
	default:
		log.Fatal("The database vendor was not defined and we didn't have a default for the database vendor!")
	}
	return nil
}

func Configuration() *configuration.Configuration {
	connectionSettings := ConnectionSettings()
	if val, exists := os.LookupEnv("DATABASE_MIGRATION_PATH"); exists {
		return &configuration.Configuration{MigrationPath: val, ConnectionSettings: connectionSettings}
	} else {
		return &configuration.Configuration{MigrationPath: "./migrations", ConnectionSettings: connectionSettings}
	}
}

func postgresConnectionSettings() postgres.ConnectionSettings {
	settings := database.Settings{Defaults: postgres.Defaults{}}
	username := settings.Username()
	host := settings.Host()
	password := settings.Password()
	options := settings.AdditionalOptions()
	return postgres.ConnectionSettings{Host: host, Vendor: postgresVendor, Username: username, Database: settings.Name(), Password: password, Port: settings.Port(), AdditionalOptions: options}
}

func sqliteConnectionSettings() sqlite3.ConnectionSettings {
	return sqlite3.ConnectionSettings{Vendor: sqliteVendor, Database: database.Name(), Username: sqlite3.GetSystemUsername()}
}
