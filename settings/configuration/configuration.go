package configuration

import "gitlab.com/nycjv321/digme/settings/connection"

type Configuration struct {
	MigrationPath      string
	ConnectionSettings connection.Settings
}
