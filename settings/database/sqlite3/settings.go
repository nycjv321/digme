package sqlite3

import (
	"log"
	"os/user"
)

func GetSystemUsername() string {
	u, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return u.Username
}
