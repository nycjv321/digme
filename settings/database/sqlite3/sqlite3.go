package sqlite3

type ConnectionSettings struct {
	Vendor   string
	Database string
	Username string
}

func (connectionSettings ConnectionSettings) Get() (string, string) {
	return connectionSettings.Vendor, "./" + connectionSettings.Database + ".db"
}

func (connectionSettings ConnectionSettings) GetUserName() string {
	return connectionSettings.Username
}

func (connectionSettings ConnectionSettings) GetVendor() string {
	return connectionSettings.Vendor
}
