package database

import (
	"log"
	"os"
	"strconv"
)

type Settings struct {
	Defaults Defaults
}

func DatabaseVendor() string {
	vendor, defined := os.LookupEnv("DATABASE_VENDOR")
	if !defined {
		return "postgres"
	} else {
		return vendor
	}
}

func (e Settings) Username() string {
	username := os.Getenv("DATABASE_USERNAME")
	if len(username) == 0 {
		return e.Defaults.Username()
	}
	return username
}

func Name() string {
	database, defined := os.LookupEnv("DATABASE_NAME")
	if !defined {
		log.Fatal("Database Name (DATABASE_NAME) not defined!")
	}
	return database
}

func (e Settings) Name() string {
	database := os.Getenv("DATABASE_NAME")
	if len(database) == 0 {
		return e.Defaults.Database()
	}
	return database
}

func (e Settings) Port() int {
	portAsString, defined := os.LookupEnv("DATABASE_PORT")
	if defined {
		if port, err := strconv.ParseInt(portAsString, 0, 32); err != nil {
			log.Fatal(err)
		} else {
			return int(port)
		}
	} else {
		return e.Defaults.Port()
	}
	return 0
}

func (e Settings) Host() string {
	host := os.Getenv("DATABASE_HOST")
	if len(host) == 0 {
		return e.Defaults.Host()
	}
	return host
}

func (e Settings) Password() string {
	password := os.Getenv("DATABASE_PASSWORD")
	if len(password) == 0 {
		return e.Defaults.Password()
	}
	return password
}

func (e Settings) AdditionalOptions() string {
	options := os.Getenv("DATABASE_ADDITIONAL_OPTIONS")
	if len(options) == 0 {
		return e.Defaults.AdditionalOptions()
	}
	return options
}
