package database

type Defaults interface {
	Username() string
	Database() string
	Host() string
	Port() int
	Password() string
	AdditionalOptions() string
}
