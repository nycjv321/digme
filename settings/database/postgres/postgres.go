package postgres

import (
	"fmt"
)

type ConnectionSettings struct {
	Vendor            string
	Database          string
	Username          string
	Host              string
	Password          string
	Port              int
	AdditionalOptions string
}

func (connectionSettings ConnectionSettings) Get() (vendor string, connectionUrl string) {
	return connectionSettings.Vendor,
		fmt.Sprintf("host=%s user=%s dbname=%s password=%s port=%d %s",
			connectionSettings.Host, connectionSettings.Username, connectionSettings.Database,
			connectionSettings.Password, connectionSettings.Port, connectionSettings.AdditionalOptions)
}

func (connectionSettings ConnectionSettings) GetUserName() string {
	return connectionSettings.Username
}

func (connectionSettings ConnectionSettings) GetVendor() string {
	return connectionSettings.Vendor
}
