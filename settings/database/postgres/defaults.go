package postgres

type Defaults struct {
}

func (Defaults) Username() string {
	return "postgres"
}

func (defaults Defaults) Database() string {
	return defaults.Username()
}

func (defaults Defaults) Host() string {
	return "127.0.0.1"
}

func (defaults Defaults) Password() string {
	return defaults.Username()
}

func (defaults Defaults) AdditionalOptions() string {
	return "sslmode=disable"
}

func (defaults Defaults) Port() int {
	return 5432
}
