package digme

import (
	"fmt"
	"sort"
	"strings"
	"time"
)

type Migration struct {
	Version   int
	Name      string
	Path	  string
	SHA256    string
	Owner     string
	Ran       time.Time
	Duration  float64
	Succeeded bool
}

// TODO retrofit binary search below in all methods
func validateChecksum(migrations []*Migration, migration *Migration) {
	for _, existingMigration := range migrations {
		if strings.Compare(existingMigration.Name, migration.Name) == 0 {
			if strings.Compare(existingMigration.SHA256, migration.SHA256) != 0 {
				panic(fmt.Sprintf("Existing migration \"%v\"'s SHA256 was different (was \"%s\" is now \"%s\")! Did someone (you?) change the file?", existingMigration.Name, existingMigration.SHA256, migration.SHA256))
			} else {
				return
			}
		}
	}
}

func isExistingMigration(migrations []*Migration, migration *Migration) (bool) {
	for _, existingMigration := range migrations {
		if strings.Compare(existingMigration.Name, migration.Name) == 0 {
			return true
		}
	}
	return false
}

func validateExistingMigrations(existingMigrations []*Migration, newMigrations []string) {
	sort.Strings(newMigrations)
	for _, migration := range existingMigrations {
		existingMigrationName := migration.Name
		i := sort.Search(len(newMigrations),
			func(i int) bool { return newMigrations[i] >= existingMigrationName })
		if i < len(newMigrations) && newMigrations[i] == existingMigrationName {
			fmt.Printf("found \"%s\" at files[%d]\n", newMigrations[i], i)
		} else {
			panic(fmt.Sprintf("previously ran migration (%v:%v) missing", existingMigrationName, migration.SHA256))
		}
	}
}
