package digme

import (
	"database/sql"
	"gitlab.com/nycjv321/digme/settings/configuration"
	is "gitlab.com/nycjv321/digme/sql"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
	"time"
)


type DBManager struct {
	db            *sql.DB
	configuration *configuration.Configuration
}



func (manager *DBManager) GetTemplates() (is.Templates) {
	vendor := manager.configuration.ConnectionSettings.GetVendor()
	if (vendor == "sqlite3") {
		return &is.SqlLiteTemplates{}
	} else {
		return &is.PostgresTemplates{}
	}
}

func (manager *DBManager) prepare(operation string) (*sql.Stmt, error) {
	return manager.db.Prepare(operation)
}

func (manager *DBManager) exec(fileName string, args ...interface{}) (sql.Result, error) {
	var operations = strings.Split(manager.readSql(fileName), ";")
	for _, operation := range operations {
		if stmt, err := manager.prepare(operation); err == nil {
			var result sql.Result
			if len(args) == 0 {
				result, err = stmt.Exec()
			} else {
				result, err = stmt.Exec(args...)
			}

			if err == nil {
				return result, err
			} else {
				log.Fatal(err)
			}
		} else {
			log.Fatal(err)
		}
		return nil, nil
	}
	return nil, nil
}


func (manager *DBManager) hasSchemaVersion() bool {

	var exists bool

	if stmt, err := manager.prepare(manager.GetTemplates().CheckSchema()); err == nil {
		row := stmt.QueryRow("migrations")
		if err := row.Scan(&exists); err == nil {
			if exists {
				log.Println("\"public.migrations\" existed!")
			} else {
				log.Println("\"public.migrations\" did not exist!")
			}
		}
		return exists
	} else {
		log.Fatal(err)
	}
	return false
}

func (manager *DBManager) createSchemaVersion() {
	if _, err := manager.exec(manager.GetTemplates().CreateSchema()); err != nil {
		log.Fatal(err)
	} else {
		log.Print("\"public.migrations\" created!")
	}
}

func (manager *DBManager) initialize() {
	if !manager.hasSchemaVersion() {
		manager.createSchemaVersion()
	}
}

func (manager *DBManager) checkSchemaVersion() (int, bool) {
	var version int
	var successful bool
	if stmt, err := manager.prepare(manager.readSql(is.GetSchemaVersion())); err == nil {
		row := stmt.QueryRow()


		if err := row.Scan(&version, &successful); err == nil || err == sql.ErrNoRows {
			log.Printf("Current schema is %v\n", version)
		} else {
			log.Fatal(err)
		}
	} else {
		log.Fatal(err)
	}
	return version, successful
}

func (manager *DBManager) newMigrations() []string {
	migrationPath := manager.configuration.MigrationPath
	if _, err := os.Stat(migrationPath); err == nil {
		if files, err := ioutil.ReadDir(migrationPath); err == nil {
			var migrations []string
			for _, file := range files {
				fileName := file.Name()
				if !file.IsDir() && strings.HasSuffix(fileName, ".sql") {
					if strings.HasPrefix(fileName, "v") {
						migrations = append(migrations, migrationPath+"/"+fileName)
					} else {
						log.Printf("\"%v\" was not prefixed with a \"v$Version!\n", fileName)
					}
				}
			}

			sort.Slice(migrations, func(i, j int) bool {
				return migrations[i] < migrations[j]
			})

			return migrations
		} else {
			log.Fatal(err)
		}
	}
	return []string{}
}

func (manager *DBManager) migrate(migrationFile string) error {
	if _, err := manager.exec(migrationFile); err != nil {
		return err
	} else {
		return nil
	}
}

func (manager *DBManager) existingMigrations() []*Migration {
	var migrations = []*Migration{}
	if stmt, err := manager.prepare(manager.readSql(is.GetExistingMigrations())); err == nil {
		if rows, err := stmt.Query(); err == nil {
			defer rows.Close()
			for rows.Next() {
				var migration Migration
				if err := rows.Scan(&migration.Version, &migration.Name, &migration.Path, &migration.SHA256, &migration.Owner, &migration.Ran, &migration.Duration, &migration.Succeeded); err == nil || err == sql.ErrNoRows {
					migrations = append(migrations, &migration)
				} else {
					log.Fatal(err)
				}
			}
			return migrations
		} else {
			log.Fatal(err)
		}
	} else {
		log.Fatal(err)
	}
	return migrations
}

func (manager *DBManager) execute() {
	version, successful := manager.checkSchemaVersion()
	if version > 0 && !successful {
		log.Fatal("Last migration was not successful. Please resolve before proceeding!")
	} else {
		newMigrations := manager.newMigrations()
		existingMigrations := manager.existingMigrations()
		if len(newMigrations) == 0 {
			log.Printf("No migrations existed in \"%v\"\n", manager.configuration.MigrationPath)
		} else {
			if len(existingMigrations) == 0 {
				log.Printf("No existing migrations")
			} else {
				validateExistingMigrations(existingMigrations, newMigrations)
			}
			// todo make below workflow more functional in nature
			for _, migration := range newMigrations {
				var ran Migration
				ran.Name =  migration
				ran.Path = manager.configuration.MigrationPath

				content := manager.readSql(migration)
				if len(content) == 0 {
					log.Printf("\"%v\" was empty.\n", migration)
					continue
				}

				ran.SHA256 = getHash(content)
				if isExistingMigration(existingMigrations, &ran) {
					validateChecksum(existingMigrations, &ran)
					continue
				}

				version = version + 1
				ran.Version = version

				ran.Owner = manager.configuration.ConnectionSettings.GetUserName()
				ran.Ran = time.Now()


				if ran.Duration == 0 {
					duration, err := measure(manager.migrate, migration)
					ran.Duration = duration.Seconds()
					ran.Succeeded = err == nil
					log.Printf("\"%v\" took \"%v\" to execute.", migration, duration)
					if !ran.Succeeded {
						log.Printf("\"%v\" failed! Took \"%v\".", migration, duration)
						log.Fatal(err)
					}
					manager.recordMigration(&ran)
				} else {
					log.Printf("Migration (%v) already Ran on %v", ran.Name, ran.Ran)
				}

			}
		}
	}
}

func (manager *DBManager) recordMigration(migration *Migration) {
	if _, err := manager.exec(manager.GetTemplates().RecordMigrations(), migration.Version, migration.Name, migration.Path, migration.SHA256, migration.Owner, migration.Ran, migration.Duration, migration.Succeeded); err != nil {
		log.Fatal(err)
	}
}
