package digme

import "testing"


func TestMigrationsStillExist(t *testing.T) {
	validateExistingMigrations([]*Migration{{Name: "test"}}, []string{"test"})
}

func TestMigrationsMissing(t *testing.T) {
	defer func() {
		if recover() == nil {
			t.Fatal("an error shouldn've been thown")
		}
	}()
	validateExistingMigrations([]*Migration{{Name: "test"}}, []string{"testing"})
}

func TestValidateChecksum(t *testing.T)  {
	validateChecksum([]*Migration{{Name: "test", SHA256: "123"}}, &Migration{Name:"test", SHA256:"123"})
}

func TestValidateChecksumFails(t *testing.T)  {
	defer func() {
		if recover() == nil {
			t.Fatal("an error shouldn've been thown")
		}
	}()
	validateChecksum([]*Migration{{Name: "test", SHA256: "321"}}, &Migration{Name:"test", SHA256:"123"})
}