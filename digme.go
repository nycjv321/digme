package digme

import (
	"database/sql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/nycjv321/digme/settings/connection/environment"
	"log"
)

func Initialize() {
	configuration := environment.Configuration()
	connectionSettings := configuration.ConnectionSettings
	db, err := sql.Open(connectionSettings.Get())
	if err != nil {
		log.Fatal(err)
	} else {
		err = db.Ping()
		if err != nil {
			log.Fatal(err)
		}

		manager := DBManager{db, configuration}
		manager.initialize()
		manager.execute()
	}
	defer db.Close()

}

func init() {
	Initialize()
}
